#include "shot.h"

#include "types/physical_units.h"

#include <iostream>
#include <numeric>

namespace PSC
{

class Shot::Impl
{
public:
	Impl();
	Impl(Cartridge const& cartridge, PelletVector const& pellets);
	Impl(Cartridge&& cartridge, PelletVector&& pellets) noexcept;

	vec3real getAvgPosition() const;
	vec3real getAvgVelocity() const;

	Cartridge _cartridge;
	Shot::PelletVector _pellets;

};


Shot::Shot()
	: _impl(std::make_unique<Impl>())
{}

Shot::~Shot() noexcept = default;
Shot::Shot(Shot &&) noexcept = default;
Shot& Shot::operator=(Shot&&) noexcept = default;

Shot::Shot(Cartridge const& cartridge, PelletVector const& pellets)
	: _impl(std::make_unique<Impl>(cartridge, pellets))
{
}

Shot::Shot(Cartridge&& cartridge, PelletVector&& pellets)
	: _impl(std::make_unique<Impl>(std::move(cartridge), std::move(pellets)))
{
}

Shot::Shot(Shot const& other)
	: _impl(std::make_unique<Impl>(*other._impl))
{
}

Shot& Shot::operator=(Shot const& other)
{
	 return *this = Shot(other);
}

Shot::PelletVector& Shot::getPellets()
{
	return _impl->_pellets;
}

Shot::PelletVector const& Shot::getPellets() const
{
	return _impl->_pellets;
}

vec3real Shot::getAvgPosition() const
{
	return _impl->getAvgPosition();
}

vec3real Shot::getAvgVelocity() const
{
	return _impl->getAvgVelocity();
}

Shot::Impl::Impl()
	: _cartridge({PelletNumber::NUMBER_7, Gram(32.0)})
{}

Shot::Impl::Impl(Cartridge const& cartridge, PelletVector const& pellets)
   : _cartridge(cartridge)
   , _pellets(pellets)
{
}

Shot::Impl::Impl(Cartridge&& cartridge, PelletVector&& pellets) noexcept
   : _cartridge(std::move(cartridge))
   , _pellets(std::move(pellets))
{   
}

vec3real Shot::Impl::getAvgPosition() const
{
	assert(!_pellets.empty());

	vec3real agg(0.0, 0.0, 0.0);
	for(auto const& pellet : _pellets)
	{
		agg += pellet.position;
	}
	return agg / static_cast<Real>(_pellets.size());
	//return std::transform_reduce(std::begin(_pellets), std::end(_pellets),
		//0.0,
		//[](auto const& lhs, auto const& rhs){return lhs+rhs;},
		//[](auto const& v){return v.mod();});
}

vec3real Shot::Impl::getAvgVelocity() const
{
	assert(!_pellets.empty());

	vec3real agg(0.0, 0.0, 0.0);
	for(auto const& pellet : _pellets)
	{
		agg += pellet.velocity;
	}
	return agg / static_cast<Real>(_pellets.size());
}
}
