#include "pellet_specs.h"

#include "constants.h"

namespace PSC
{
	
constexpr PelletSpecs getPelletSpecs(Real diameter, Real weight)
{
	return PelletSpecs(diameter, 
						weight,
						Constants::pi<Real> * diameter * diameter / (8.0 * weight) );
}

constexpr std::array<PelletSpecs, static_cast<size_t>(PelletNumber::COUNT)>
data = {
	// mm / 1000 -> meters , grams
	getPelletSpecs(1.5 / 1000, 0.01960784314),			// 11
	getPelletSpecs(1.75 / 1000, 0.03105590062),		// 10
	getPelletSpecs(2 / 1000, 0.04651162791),			// 9
	getPelletSpecs(2.25 / 1000, 0.06622516556),		// 8
	getPelletSpecs(2.37 / 1000, 0.07751937984),		// 7.5
	getPelletSpecs(2.5 / 1000, 0.09090909091),			// 7
	getPelletSpecs(2.75 / 1000, 0.1204819277),			// 6
	getPelletSpecs(3 / 1000, 0.15625),					// 5
	getPelletSpecs(3.25 / 1000, 0.2),						// 4
	getPelletSpecs(3.5 / 1000, 0.25),						// 3
	getPelletSpecs(3.75 / 1000, 0.303030303),			// 2
	getPelletSpecs(4 / 1000, 0.3703703704),				// 1
	getPelletSpecs(4.25 / 1000, 0.4545454545),			// 0
	getPelletSpecs(4.5 / 1000, 0.5263157895),			// 00
	getPelletSpecs(4.75 / 1000, 0.625),					// 000
	getPelletSpecs(6.2 / 1000, 1.385055185),			// SLUG 6.2
	getPelletSpecs(6.8 / 1000, 1.827337417),			// SLUG 6.8
	getPelletSpecs(7.4 / 1000, 2.354979701),			// SLUG 7.4
	getPelletSpecs(7.68 / 1000, 2.591624546),			// SLUG 7.68
	getPelletSpecs(8.65 / 1000, 3.761320415)			// SLUG 8.65
};

PelletSpecs const& PelletsSpecs::get(PelletNumber number)
{
	return data[static_cast<size_t>(number)];
}

}
