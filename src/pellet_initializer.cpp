#include "pellet_initializer.h"

#include "cartridge.h"
#include "flying_object.h"
#include "random/randomizer.h"
#include "utils/geometric.h"

namespace PSC
{
using namespace Random;
using namespace Utils;


PelletInitializer::PelletInitializer(Cartridge const& cartridge, vec3real direction) noexcept
	: _cartridge{cartridge}
	, _direction{direction}
{}

FlyingObject PelletInitializer::initializePellet(Random::Randomizer& randomizer) const
{
	auto rho = randomizer.getRho(); // Convert to meters
	auto theta = randomizer.getTheta();
	auto deformationFactor = randomizer.getDeformationFactor(rho);
	
	// Get a random point in the plane with by the direction vector as the normal
	// vector defining it
	// For that, pick a vector and rotate a random angle theta, then set the distance
	// to the center witha  random value rho
	auto randomPointInPlane = getVectorRotatedInPlane(_direction, theta);
	auto pointInPlane = randomPointInPlane.norm() * rho;

	// Now move the point to the reference we have 40yards
	movePoint(_direction, pointInPlane, Constants::referenceDistance40Yards<Real>);

	auto velocity = pointInPlane.norm() * _cartridge.getInitialSpeed();
	auto dragCoeficient = _cartridge.getDragCoefficientMaterialFactor() * deformationFactor;
	auto position = vec3real(0.0, 0.0, 0.0);
	return {position, velocity, dragCoeficient};
}

}
