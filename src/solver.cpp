#include "solver.h"

#include "constants.h"

#include <algorithm>
#include <iostream>

namespace PSC
{
struct Solver::Impl
{
	Impl(Shot shot)
		: _shot(shot)
		, _dt(0.0), _currentTime(0.0), _currentDistance(0.0), _currentVelocity(0.0) {}

	Shot const* solveStep(Real dt);
	Solver::Storage solveFullPath();

	static vec3real getDragForceAcceleration(FlyingObject const& pellet);
	static vec3real getGravityAcceleration(FlyingObject const& pellet);
	void advanceStepTransform(Real dt);

	bool isStopConditionDefined() const noexcept;
	bool checkDefinedStopCondition() const noexcept;
	bool defaultStopCondition() const noexcept;

	std::function<bool()> getStopFunc() const noexcept;

	Shot _shot;

	Real _dt;
	Real _currentTime;
	Real _currentDistance;
	Real _currentVelocity;

	std::function<bool(Real)> _distanceStopCondition;
	std::function<bool(Real)> _velocityStopCondition;
	std::function<bool(Real)> _timeStopCondition;

};

Solver::Solver(Shot shot)
	: _impl(std::make_unique<Solver::Impl>(shot))
{}

Solver::~Solver() noexcept = default;
Solver::Solver(Solver&&) noexcept = default;
Solver& Solver::operator=(Solver&&) noexcept = default;

Solver::Solver(Solver const& other)
	: _impl(std::make_unique<Impl>(*other._impl))
{
}

Solver& Solver::operator=(Solver const& other)
{
	return *this = Solver(other);
}


void Solver::setDistanceStopCondition(std::function<bool(Real)> conditionFunc)
{
	_impl->_distanceStopCondition = conditionFunc;
}

void Solver::setVelocityStopCondition(std::function<bool(Real)> conditionFunc)
{
	_impl->_velocityStopCondition = conditionFunc;
}

void Solver::setTimeStopCondition(std::function<bool(Real)> conditionFunc)
{
	_impl->_timeStopCondition = conditionFunc;
}

void Solver::setTimeFraction(Real time)
{
	_impl->_dt = time;
}

Solver::Storage Solver::solveFullPath()
{
	return _impl->solveFullPath();
}

Shot const* Solver::solveStep(Real dt)
{
	return _impl->solveStep(dt);
}

bool Solver::Impl::isStopConditionDefined() const noexcept
{
	return _distanceStopCondition || _velocityStopCondition || _timeStopCondition;
}

bool Solver::Impl::checkDefinedStopCondition() const noexcept
{
	return (_distanceStopCondition && _distanceStopCondition(_currentDistance)) ||
			 (_velocityStopCondition && _velocityStopCondition(_currentVelocity)) ||
			 (_timeStopCondition && _timeStopCondition(_currentTime));
}

bool Solver::Impl::defaultStopCondition() const noexcept
{
	return _currentVelocity < 20.0 || _currentTime > 1.0 || _currentDistance > 100.0;
}

std::function<bool()> Solver::Impl::getStopFunc() const noexcept
{
	if (isStopConditionDefined())
	{
		return [this]() -> bool { return this->checkDefinedStopCondition(); };
	}
	else
	{
		return [this]() -> bool { return this->defaultStopCondition(); };
	}
}

Solver::Storage Solver::Impl::solveFullPath()
{
	Storage storage;

	_currentDistance += _shot.getAvgPosition().mod();
	_currentVelocity = _shot.getAvgVelocity().mod();
	_dt = (_dt > 0.000001) ? _dt : 0.5 / _currentVelocity;

	auto stopChecking = getStopFunc();
	while(!stopChecking())
	{
		advanceStepTransform(_dt);
		_currentTime += _dt;
		_currentDistance = _shot.getAvgPosition().mod();
		_currentVelocity = _shot.getAvgVelocity().mod();

		storage.emplace_back(_currentTime,
									_currentDistance,
									_currentVelocity,
									_shot);
	}

	return storage;
}

Shot const* Solver::Impl::solveStep(Real dt)
{
	auto stopChecking = getStopFunc();
	advanceStepTransform(dt);
	_currentTime += dt;
	_currentDistance = _shot.getAvgPosition().mod();
	_currentVelocity = _shot.getAvgVelocity().mod();

	return stopChecking() ? nullptr : &_shot;
}

void Solver::Impl::advanceStepTransform(Real dt)
{
	auto& pellets = _shot.getPellets();
	std::transform(std::begin(pellets), std::end(pellets), std::begin(pellets),
	[dt](auto const& pellet)
	{
		FlyingObject ret {pellet};
		ret.velocity += (Solver::Impl::getDragForceAcceleration(pellet) +
							  Solver::Impl::getGravityAcceleration(pellet))
							  * dt;
		ret.position += pellet.velocity * dt;
		return ret;
	});
}

vec3real Solver::Impl::getDragForceAcceleration(FlyingObject const& pellet)
{
	auto velocityMod = pellet.velocity.mod();
	auto march = velocityMod / Constants::soundSpeed<Real>;
	auto cd = (march < Constants::transonicMarchLimit<Real>) ?
				Constants::subsonicCd<Real> :
				march * Constants::supersonicCdSlope<Real>;

	auto dv = -1 * cd * Constants::airDensity<Real> * pellet.dragCoeficient
			* std::pow(velocityMod, 2);

	return pellet.velocity * (dv / velocityMod);
}

vec3real Solver::Impl::getGravityAcceleration(FlyingObject const& /*pellet*/)
{
	static vec3real gravityAxis = {0, 0, -1};
	return gravityAxis * Constants::gravity<Real>;
}

}
