#include "random/randomizer.h"
#include "constants.h"

namespace PSC
{
namespace Random
{

Randomizer::Randomizer(Real scale)
	: _rayleigh{scale}
#ifdef USE_FIXED_SEED
	, _gen{FIXED_SEED}
#else
	, _gen{std::random_device()()}
#endif
	, _uniform{0.0, 2.0*Constants::pi<Real>}
	, _scale{scale}
{}

void Randomizer::setScale(Real scale) noexcept
{
	_rayleigh = RayleighDistribution<Real>(scale);
}

}
}
