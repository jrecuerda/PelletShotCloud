#include "shot_factory.h"

#include "constants.h"
#include "pellet_initializer.h"
#include "random/randomizer.h"

#include <algorithm>
#include <memory>

namespace PSC
{

std::vector<FlyingObject> buildPellets(size_t numPellets, PelletInitializer& pelletInitializer)
{
	static std::unique_ptr<Random::Randomizer> randomizer;
	if(!randomizer)
	{
		randomizer = std::make_unique<Random::Randomizer>(Constants::referenceStd40Yards3Stars<Real>);
	}

	std::vector<FlyingObject> pellets(numPellets);
	std::generate(std::begin(pellets), std::end(pellets),
		[&randomizerRef = *randomizer, &pelletInitializer]
		{
			return pelletInitializer.initializePellet(randomizerRef);
		});

	return pellets;
}

Shot ShotFactory::create(Cartridge cartridge, vec3real direction)
{   
	PelletInitializer pelletInitializer(cartridge, direction);
	return Shot(std::move(cartridge),
					buildPellets(cartridge.getNumberOfPellets(), pelletInitializer));
}

}

