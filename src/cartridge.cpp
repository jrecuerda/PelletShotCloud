#include "cartridge.h"
#include "pellet_specs.h"

namespace PSC
{
	unsigned int Cartridge::getNumberOfPellets() const noexcept
	{
		return _numberOfPellets;
	}

	Real Cartridge::getInitialSpeed() const noexcept
	{
		return _initialSpeed;
	}

	Real Cartridge::getDragCoefficientMaterialFactor() const noexcept
	{
		return _pelletSpecs.dragCoefficientMaterialFactor;
	}

}
