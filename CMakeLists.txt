cmake_minimum_required(VERSION 3.0)
project(PelletShotCloud VERSION 0.0.1 LANGUAGES CXX)

option(BUILD_EXAMPLES "Build examples" ON)

option(BUILD_STATIC_LIBS "Build the static library" ON)
option(BUILD_SHARED_LIBS "Build the shared library" OFF)
option(BUILD_TESTS "Build test programs" ON)
option(USE_FIXED_SEED "Use fixed random seed" OFF)
option(REAL_AS_FLOAT "Use float instead of double for floating point numbers" OFF)
set(FIXED_SEED 1)

set(ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_INSTALL_PREFIX}/lib)
set(LIBRARY_OUTPUT_DIRECTORY ${CMAKE_INSTALL_PREFIX}/lib)
set(RUNTIME_OUTPUT_DIRECTORY ${CMAKE_INSTALL_PREFIX}/bin)
set(INCLUDE_OUTPUT_DIRECTORY ${CMAKE_INSTALL_PREFIX}/include)

set(INCLUDE_DIR_PSC ${CMAKE_SOURCE_DIR}/include/pelletshotcloud)

set_property(GLOBAL PROPERTY CXX_STANDARD 17)

add_subdirectory(src)

if(${BUILD_EXAMPLES})
	add_subdirectory(examples)
endif(${BUILD_EXAMPLES})

# Test values are saved for double by now
if(${BUILD_TESTS} AND NOT ${REAL_AS_FLOAT})
	add_subdirectory(test)
endif(${BUILD_TESTS} AND NOT ${REAL_AS_FLOAT})
