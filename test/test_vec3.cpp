#include "catch2/catch2.hpp"

#include "pelletshotcloud/types/vec3.h"

using namespace PSC;

TEST_CASE("Different real types", "[types]")
{
	auto vecfloat = vec3<float>();
	CHECK(vecfloat.x == Approx(0.0f));
	CHECK(vecfloat.y == Approx(0.0f));
	CHECK(vecfloat.z == Approx(0.0f));

	auto vecdouble = vec3<double>();
	CHECK(vecdouble.x == Approx(0.0));
	CHECK(vecdouble.y == Approx(0.0));
	CHECK(vecdouble.z == Approx(0.0));
}

TEST_CASE("Vec3 mod", "[types]")
{
	auto vec1 = vec3<float>(1.f, 2.f, 3.f);
	CHECK(vec1.mod() == Approx(3.7416573868));
}

TEST_CASE("Vec3 norm", "[types]")
{
	auto vec1 = vec3<float>(1.f, 2.f, 3.f);
	auto res = vec1.norm();
	CHECK(res.x == Approx(0.2672612419));
	CHECK(res.y == Approx(0.5345224838));
	CHECK(res.z == Approx(0.8017837257));

	CHECK(res.mod() == Approx(1.f));
}

TEST_CASE("Vec3 dot", "[types]")
{
	auto vec1 = vec3<float>(1.f, 2.f, 3.f);
	auto vec2 = vec3<float>(3.f, 2.f, 1.f);
	auto res = vec1.dot(vec2);
	CHECK(res == Approx(10.f));
}

TEST_CASE("Vec3 cross", "[types]")
{
	auto vec1 = vec3<float>(1.f, 2.f, 3.f);
	auto vec2 = vec3<float>(3.f, 2.f, 1.f);
	auto res = vec3<float>::cross(vec1, vec2);
	CHECK(res.x == Approx(-4.f));
	CHECK(res.y == Approx(8.f));
	CHECK(res.z == Approx(-4.f));
}

TEST_CASE("Vec3 const arithmetic", "[types]")
{
	auto vec1 = vec3<float>(1.f, 2.f, 3.f);
	auto vec2 = vec3<float>(1.f, 2.f, 3.f);

	vec3<float> vecRes;

	SECTION("Operation +")
	{
		vecRes = vec1 + vec2;
		CHECK(vecRes.x == Approx(2.f));
		CHECK(vecRes.y == Approx(4.f));
		CHECK(vecRes.z == Approx(6.f));
	}

	SECTION("Operation -")
	{
		vecRes = vec1 - vec2;
		CHECK(vecRes.x == Approx(0.f));
		CHECK(vecRes.y == Approx(0.f));
		CHECK(vecRes.z == Approx(0.f));
	}

	SECTION("Operation *")
	{
		vecRes = vec1 * vec2;
		CHECK(vecRes.x == Approx(1.f));
		CHECK(vecRes.y == Approx(4.f));
		CHECK(vecRes.z == Approx(9.f));
	}

	SECTION("Operation /")
	{
		vecRes = vec1 / vec2;
		CHECK(vecRes.x == Approx(1.f));
		CHECK(vecRes.y == Approx(1.f));
		CHECK(vecRes.z == Approx(1.f));
	}

	SECTION("Operation +2")
	{
		vecRes = vec1 + 2.f;
		CHECK(vecRes.x == Approx(3.f));
		CHECK(vecRes.y == Approx(4.f));
		CHECK(vecRes.z == Approx(5.f));
	}

	SECTION("Operation -2")
	{
		vecRes = vec1 - 2.f;
		CHECK(vecRes.x == Approx(-1.f));
		CHECK(vecRes.y == Approx(0.f));
		CHECK(vecRes.z == Approx(1.f));
	}

	SECTION("Operation *2")
	{
		vecRes = vec1 * 2.f;
		CHECK(vecRes.x == Approx(2.f));
		CHECK(vecRes.y == Approx(4.f));
		CHECK(vecRes.z == Approx(6.f));
	}

	SECTION("Operation /2")
	{
		vecRes = vec1 / 2.f;
		CHECK(vecRes.x == Approx(0.5f));
		CHECK(vecRes.y == Approx(1.f));
		CHECK(vecRes.z == Approx(1.5f));
	}

	SECTION("Operation +1 *4 /2 -2")
	{
		vecRes = vec1 + 1.f * 4.f / 2.f - 2.f;
		CHECK(vecRes.x == Approx(1.f));
		CHECK(vecRes.y == Approx(2.f));
		CHECK(vecRes.z == Approx(3.f));
	}

}
TEST_CASE("Vec3 arithmetic", "[types]")
{
	auto vec1 = vec3<float>(1.f, 2.f, 3.f);
	auto vec2 = vec3<float>(1.f, 2.f, 3.f);

	SECTION("Operation += vec2")
	{
		vec1 += vec2;
		CHECK(vec1.x == Approx(2.f));
		CHECK(vec1.y == Approx(4.f));
		CHECK(vec1.z == Approx(6.f));
	}

	vec1.set(1.f, 2.f, 3.f);

	SECTION("Operation -= vec2")
	{
		vec1 -= vec2;
		CHECK(vec1.x == Approx(0.f));
		CHECK(vec1.y == Approx(0.f));
		CHECK(vec1.z == Approx(0.f));
	}

	vec1.set(1.f, 2.f, 3.f);

	SECTION("Operation *= vec2")
	{
		vec1 *= vec2;
		CHECK(vec1.x == Approx(1.f));
		CHECK(vec1.y == Approx(4.f));
		CHECK(vec1.z == Approx(9.f));
	}

	vec1.set(1.f, 2.f, 3.f);

	SECTION("Operation /= vec2")
	{
		vec1 /= vec2;
		CHECK(vec1.x == Approx(1.f));
		CHECK(vec1.y == Approx(1.f));
		CHECK(vec1.z == Approx(1.f));
	}

	vec1.set(1.f, 2.f, 3.f);

	SECTION("Operation += vec2*vec2 ")
	{
		vec1 += vec2*vec2;
		CHECK(vec1.x == Approx(2.f));
		CHECK(vec1.y == Approx(6.f));
		CHECK(vec1.z == Approx(12.f));
	}

}
