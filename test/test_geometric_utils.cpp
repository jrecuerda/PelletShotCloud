#include "catch2/catch2.hpp"

#include "types/psc.h"
#include "pelletshotcloud/utils/geometric.h"
#include "pelletshotcloud/constants.h"

#include <vector>

using namespace PSC;
using namespace PSC::Utils;
using namespace PSC::Constants;

TEST_CASE("Rotation", "[utils]")
{
	auto axis = vec3<Real>{1.0, 1.0, 1.0};
	auto vectorToRotate = vec3<Real>::cross(axis, vec3<Real>(0.0, 0.0, 1.0));
	// Vectors must be perpendicular since we pick it up by doing the cross product
	CHECK(axis.dot(vectorToRotate) == Approx(0.0));

	SECTION("PI/2 rotation")
	{
		auto res = rotateVector<Real>(axis, vectorToRotate, pi<Real>*0.5);
		CHECK(res.x == Approx(1.0));
		CHECK(res.y == Approx(1.0));
		CHECK(res.z == Approx(-2.0));
	}
	SECTION("25degrees rotation")
	{
		auto res = rotateVector<Real>(axis, vectorToRotate, 0.436332313);
		CHECK(res.x == Approx(1.328926049));
		CHECK(res.y == Approx(-0.4836895253));
		CHECK(res.z == Approx(-0.8452365235));
	}
}

TEST_CASE("Pick any perpendicular vector", "[utils]")
{
	std::vector<vec3real> vecs = {
		{1.0, 0.0, 0.0},
		{0.0, 0.0, 1.0},
		{1.0, 0.0, 1.0},
		{0.0, 1.0, 1.0},
		{1.0, 1.0, 0.0},
		{1.0, 1.0, 1.0},
		{-1.0, -1.0, -1.0},
		{0.1, 0.1, 0.1}
	};

	std::for_each(std::begin(vecs), std::end(vecs),
					[](auto const& vec)
	{
		auto perp = pickAnyPerpendicularVector(vec);
		CHECK(perp.dot(vec) == Approx(0.0));
	});
}


TEST_CASE("Vector rotated in plane", "[utils]")
{
	auto axis = vec3real{1.0, 1.0, 1.0};
	auto res = getVectorRotatedInPlane<Real>(axis, pi<Real>*0.5);

	CHECK(res.x == Approx(1.0));
	CHECK(res.y == Approx(1.0));
	CHECK(res.z == Approx(-2.0));
}

TEST_CASE("Move point", "[utils]")
{
	auto point = vec3real(0.0, 0.0, 0.0);

	movePoint<Real>(vec3real{1.0, 0.0, 0.0}, point, 2.0);
	CHECK(point.x == Approx(2.0));
	CHECK(point.y == Approx(0.0));
	CHECK(point.z == Approx(0.0));

	movePoint<Real>(vec3real{0.0, 0.1, 0.0}, point, 2.0);
	CHECK(point.x == Approx(2.0));
	CHECK(point.y == Approx(2.0));
	CHECK(point.z == Approx(0.0));

	movePoint<Real>(vec3real{0.0, 0.0, 100.0}, point, 2.0);
	CHECK(point.x == Approx(2.0));
	CHECK(point.y == Approx(2.0));
	CHECK(point.z == Approx(2.0));
}
