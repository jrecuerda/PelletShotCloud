#include "catch2/catch2.hpp"

#include "pelletshotcloud/cartridge.h"
#include "pelletshotcloud/constants.h"
#include "pelletshotcloud/random/randomizer.h"
#include "pelletshotcloud/flying_object.h"
#include "pelletshotcloud/pellet_initializer.h"

using namespace PSC;

#ifdef USE_FIXED_SEED
TEST_CASE("Pellet Initializer", "")
{
	Cartridge cartridge(PelletNumber::NUMBER_7_5, Gram(32.0));
	auto direction = vec3real(1.0, 1.0, 1.0);

	PelletInitializer initializer(cartridge, direction);

    Random::Randomizer randomizer {Constants::referenceStd40Yards3Stars<Real>};
    auto pellet = initializer.initializePellet(randomizer);

#ifdef WIN32 
	CHECK(pellet.velocity.x == Approx(254.1003951869));
	CHECK(pellet.velocity.y == Approx(253.8588007813));
	CHECK(pellet.velocity.z == Approx(254.1430668575));
	CHECK(pellet.dragCoeficient == Approx(0.0000279804));
#else
	CHECK(pellet.velocity.x == Approx(254.1003951869));
	CHECK(pellet.velocity.y == Approx(253.8588007813));
	CHECK(pellet.velocity.z == Approx(254.1430668575));
	CHECK(pellet.dragCoeficient == Approx(0.0000283675));
#endif
	
}
#endif
