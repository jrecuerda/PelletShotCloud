#include "catch2/catch2.hpp"

#include "pelletshotcloud/cartridge.h"
#include "pelletshotcloud/types/physical_units.h"

using namespace PSC;

TEST_CASE("Cartridge")
{
	Cartridge cartridge32(PelletNumber::NUMBER_7, Gram(32.0));
	CHECK(cartridge32.getNumberOfPellets() == 351);

	Cartridge cartridge36(PelletNumber::NUMBER_7, Gram(36.0));
	CHECK(cartridge36.getNumberOfPellets() == 395);

	Cartridge cartridge36_Kg(PelletNumber::NUMBER_7, Kilogram(0.036));
	CHECK(cartridge36_Kg.getNumberOfPellets() == 395);

	cartridge36_Kg.setInitialSpeed(MeterPerSecond(440.0));
}
