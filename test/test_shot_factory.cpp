#include "catch2/catch2.hpp"

#include "pelletshotcloud/cartridge.h"
#include "pelletshotcloud/shot_factory.h"
#include "pelletshotcloud/types/vec3.h"

using namespace PSC;

TEST_CASE("ShotFactory")
{
	Cartridge cartridge(PelletNumber::NUMBER_7, Gram(32.0));
	Shot shot = ShotFactory::create(cartridge, vec3real(1.0, 1.0, 1.0));
	
	auto const& pellets = shot.getPellets();
	
	CHECK(cartridge.getNumberOfPellets() == pellets.size());
#ifdef USE_FIXED_SEED
#ifdef WIN32 
	CHECK(pellets[0].velocity.x == Approx(254.1003951869));
	CHECK(pellets[0].velocity.y == Approx(253.8588007813));
	CHECK(pellets[0].velocity.z == Approx(254.1430668575));
	CHECK(pellets[0].dragCoeficient == Approx(0.0000265485));
#else
	CHECK(pellets[0].velocity.x == Approx(254.1003951869));
	CHECK(pellets[0].velocity.y == Approx(253.8588007813));
	CHECK(pellets[0].velocity.z == Approx(254.1430668575));
	CHECK(pellets[0].dragCoeficient == Approx(0.0000269158));
#endif
#endif
}
