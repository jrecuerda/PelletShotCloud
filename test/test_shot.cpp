#include "catch2/catch2.hpp"

#include "pelletshotcloud/cartridge.h"
#include "pelletshotcloud/shot.h"
#include "types/vec3.h"

using namespace PSC;

TEST_CASE("Shot")
{
	Cartridge cartridge(PelletNumber::NUMBER_7, Gram(32.0));
	Shot::PelletVector pellets = {FlyingObject(), FlyingObject(), FlyingObject()};

	SECTION("0.0 average")
	{
		Shot shot(cartridge, pellets);

		CHECK(shot.getAvgPosition().x == Approx(0.0));
		CHECK(shot.getAvgPosition().y == Approx(0.0));
		CHECK(shot.getAvgPosition().z == Approx(0.0));

		CHECK(shot.getAvgVelocity().x == Approx(0.0));
		CHECK(shot.getAvgVelocity().y == Approx(0.0));
		CHECK(shot.getAvgVelocity().z == Approx(0.0));
	}

	SECTION("0.0 average")
	{
		pellets[0].position = vec3real(1.0, 1.0, 1.0);
		pellets[1].position = vec3real(2.0, 2.0, 2.0);
		pellets[2].position = vec3real(3.0, 3.0, 3.0);

		pellets[0].velocity = vec3real(1.0, 1.0, 1.0);
		pellets[1].velocity = vec3real(2.0, 2.0, 2.0);
		pellets[2].velocity = vec3real(3.0, 3.0, 3.0);

		Shot shot(cartridge, pellets);

		CHECK(shot.getAvgPosition().x == Approx(2.0));
		CHECK(shot.getAvgPosition().y == Approx(2.0));
		CHECK(shot.getAvgPosition().z == Approx(2.0));

		CHECK(shot.getAvgVelocity().x == Approx(2.0));
		CHECK(shot.getAvgVelocity().y == Approx(2.0));
		CHECK(shot.getAvgVelocity().z == Approx(2.0));
	}


}
