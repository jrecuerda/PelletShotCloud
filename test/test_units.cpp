#include "catch2/catch2.hpp"
#include "pelletshotcloud/types/physical_units.h"

using namespace PSC; 

TEST_CASE("Units", "[types]")
{
	SECTION("Weight")
	{
		auto weight = Gram(1.0);
		Real value = weight.get_is();
		CHECK(value == Approx(1));

		auto weightKg = Kilogram(2.0);
		value = weightKg.get_is();
		CHECK(value == Approx(2000.0));

		auto weightmg = Milligram(1.5);
		value = weightmg.get_is();
		CHECK(value == Approx(0.0015));
	}

	SECTION("Distance")
	{
		auto distance = Meter(1.0);
		Real value = distance.get_is();
		CHECK(value == Approx(1));

		auto kilometer = Kilometer(2.0);
		value = kilometer.get_is();
		CHECK(value == Approx(2000.0));

		auto centimeter = Centimeter(10);
		value = centimeter.get_is();
		CHECK(value == Approx(0.1));

		auto millimeter = Millimeter(1.5);
		value = millimeter.get_is();
		CHECK(value == Approx(0.0015));
	}

	SECTION("Area")
	{
		auto distance = MeterSquared(1.0);
		Real value = distance.get_is();
		CHECK(value == Approx(1));

		auto kilometer = KilometerSquared(2.0);
		value = kilometer.get_is();
		CHECK(value == Approx(2000000.0));

		auto centimeter = CentimeterSquared(10);
		value = centimeter.get_is();
		CHECK(value == Approx(0.001));

		auto millimeter = MillimeterSquared(1000);
		value = millimeter.get_is();
		CHECK(value == Approx(0.001));
	}

	SECTION("Time")
	{
		auto seconds = Second(1.0);
		Real value = seconds.get_is();
		CHECK(value == Approx(1));

		auto milliseconds = Millisecond(2.0);
		value = milliseconds.get_is();
		CHECK(value == Approx(0.002));

		auto microseconds = Microsecond(20.0);
		value = microseconds.get_is();
		CHECK(value == Approx(0.002));

		auto minutes = Minute(10);
		value = minutes.get_is();
		CHECK(value == Approx(600));

		auto hours = Hour(1.0);
		value = hours.get_is();
		CHECK(value == Approx(3600.0));
	}

	SECTION("Speed")
	{
		auto metersPerSecond = MeterPerSecond(12.2);
		Real value = metersPerSecond.get_is();
		CHECK(value == Approx(12.2));

		auto kilometersPerhour = KilometerPerHour(2.0);
		value = kilometersPerhour.get_is();
		CHECK(value == Approx(0.55555555));
	}

	SECTION("Pressure")
	{
		auto gramPerMeterCubic = GramPerMeterCubic(2.2);
		Real value = gramPerMeterCubic.get_is();
		CHECK(value == Approx(2.2));

		auto gramPerMilliMeterCubic = GramPerMilliMeterCubic(2.0);
		value = gramPerMilliMeterCubic.get_is();
		CHECK(value == Approx(2000000000.0));
	}
}
