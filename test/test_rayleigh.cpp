#include "catch2/catch2.hpp"

#include "pelletshotcloud/random/rayleigh.h"

using namespace PSC::Random;

TEST_CASE("Rayleigh", "[random]")
{
	using Rayleigh = RayleighDistribution<>;
	SECTION("Fixed seed")
	{
		std::mt19937 gen;
		RayleighDistribution<> dist(100.0);
		CHECK(dist(gen) == Approx(199.9476613042));
	}

	SECTION("Fixed seed set")
	{
		std::mt19937 gen{50};
		RayleighDistribution<> dist(100.0);
		CHECK(dist(gen) == Approx(202.3161108134));
	}

	SECTION("Random seed")
	{
		std::random_device rd{};
		std::mt19937 gen{rd()};
		RayleighDistribution<> dist(100.0);
		// If it returns this exact number I will gamble the lotery
		CHECK(dist(gen) != Approx(199.9476613042));
	}
}
