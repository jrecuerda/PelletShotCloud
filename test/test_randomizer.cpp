#include "catch2/catch2.hpp"

#include <pelletshotcloud/random/randomizer.h>

using namespace PSC::Random;

#ifdef USE_FIXED_SEED
TEST_CASE("Randomizer", "[random]")
{
	Randomizer randomizer(220.0);

#ifdef WIN32 
	CHECK(randomizer.getRho() == Approx(16.5195299416));
	CHECK(randomizer.getTheta() == Approx(5.8594307111));
	CHECK(randomizer.getDeformationFactor(50.0) == Approx(0.9929053629));
#else
	CHECK(randomizer.getRho() == Approx(16.5195299416));
	CHECK(randomizer.getTheta() == Approx(5.8594307111));
	CHECK(randomizer.getDeformationFactor(50.0) == Approx(1.0077457584));
#endif
}
#endif
