#include "catch2/catch2.hpp"

#include "pelletshotcloud/pellet_specs.h"

using namespace PSC;

TEST_CASE("Pellets Specs")
{
	CHECK(PelletsSpecs::get(PelletNumber::NUMBER_7_5).diameter == 0.00237);
	CHECK(PelletsSpecs::get(PelletNumber::NUMBER_7_5).weight == Approx(0.07751937984));
	CHECK(PelletsSpecs::get(PelletNumber::NUMBER_7_5).dragCoefficientMaterialFactor == Approx(0.00002845419399));
}

