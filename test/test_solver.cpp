#include "catch2/catch2.hpp"

#include "pelletshotcloud/cartridge.h"
#include "pelletshotcloud/shot.h"
#include "pelletshotcloud/solver.h"

#include <iostream>

using namespace PSC;

TEST_CASE("Solver")
{
	Cartridge cartridge(PelletNumber::NUMBER_7, Gram(32.0));
	FlyingObject pellet{{0.0, 0.0, 0.0},
							  {254.0341184434, 254.0341184434, 254.0341184434},
							  3e-5};
	Shot shot;
	shot.getPellets().push_back(pellet);
	SECTION("Full Path")
	{
		Solver solver(shot);
		auto storage = solver.solveFullPath();

		CHECK(storage.size() == 773);
		CHECK(storage.front().time == Approx(0.0011363636));
		CHECK(storage[99].time == Approx(0.11363636));
		CHECK(storage.back().time == Approx(0.8784090909));


		CHECK(storage.front().distance == Approx(0.5));
		CHECK(storage[99].distance == Approx(30.9306894386));
		CHECK(storage.back().distance == Approx(100.037578679));

		CHECK(storage.front().velocity == Approx(431.2583169487));
		CHECK(storage[99].velocity == Approx(195.2050936208));
		CHECK(storage.back().velocity == Approx(48.0516379869));
	}

	SECTION("Step Path")
	{
		Solver solver(shot);
		auto dt = 0.0011363636;

		auto currShot = solver.solveStep(dt);

		int iterations = 0;
		while(currShot)
		{
			if(iterations == 0)
			{
				CHECK(currShot->getAvgPosition().mod() == Approx(0.5));
				CHECK(currShot->getAvgVelocity().mod() == Approx(431.2583169487));
			}
			else if(iterations == 99)
			{
				CHECK(currShot->getAvgPosition().mod() == Approx(30.9306894386));
				CHECK(currShot->getAvgVelocity().mod() == Approx(195.2050936208));
			}

			++iterations;
			currShot = solver.solveStep(dt);
		}
		CHECK(iterations == 772);
	}
}
