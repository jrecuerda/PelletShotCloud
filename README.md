Pellet Shot Cloud
======================

[![pipeline status](https://gitlab.com/jrecuerda/PelletShotCloud/badges/master/pipeline.svg)](https://gitlab.com/jrecuerda/PelletShotCloud/commits/master)

Created by Jesus Recuerda

PelletShotCloud is a shotgun shot simulator which uses as reference the thesis submitted by David John Compton, for the Degree of Doctor of Philosophy in the University of London, **An experimental and theoretical investigation of shot cloud ballistics**
