#include <iostream>
#include <chrono>

#include "pelletshotcloud/cartridge.h"
#include "pelletshotcloud/shot_factory.h"
#include "pelletshotcloud/solver.h"
#include "pelletshotcloud/types/vec3.h"


using namespace PSC;

int main(int argc, char* argv[])
{
	using namespace PSC;
	std::cout << "-----------Example-------------" << "\n";
	auto start = std::chrono::steady_clock::now();

	Cartridge cartridge(PelletNumber::NUMBER_7_5, Gram(32.0));

	for(int i = 0; i < 10000; i++)
	{
		Shot shot = ShotFactory::create(cartridge, vec3real(1.0, 1.0, 1.0));

		Solver solver(shot);
		solver.setTimeFraction(0.006);
		auto storage = solver.solveFullPath();
		if(i == 0)
		{
		    std::cout << "First sim distance: " << storage.back().distance << "\n";
      }
	}
	auto elapsed = std::chrono::steady_clock::now() - start;
	std::cout << "TIME: " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << "\n";

	return 0;
}
