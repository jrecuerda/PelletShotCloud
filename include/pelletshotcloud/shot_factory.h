#pragma once

#include "shot.h"

#include <memory>

namespace PSC
{
class ShotFactory
{
public:
   static Shot create(Cartridge cartridge, vec3real direction);
private:
};
}