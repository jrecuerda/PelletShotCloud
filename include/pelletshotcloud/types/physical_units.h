#pragma once

#include "types/units.h"

namespace PSC
{
	// Weight
	using Gram = Quantity<std::ratio<1>, std::ratio<0>, std::ratio<0>>;
	using Milligram = Quantity<std::ratio<1,1000>, std::ratio<0>, std::ratio<0>>;
	using Kilogram = Quantity<std::ratio<1000>, std::ratio<0>, std::ratio<0>>;

	// Distance 
	using Meter = Quantity<std::ratio<0>, std::ratio<1>, std::ratio<0>>;
	using Millimeter = Quantity<std::ratio<0>, std::ratio<1,1000>, std::ratio<0>>;
	using Centimeter = Quantity<std::ratio<0>, std::ratio<1,100>, std::ratio<0>>;
	using Kilometer = Quantity<std::ratio<0>, std::ratio<1000>, std::ratio<0>>;

	// Area
	using MeterSquared = Quantity<std::ratio<0>, std::ratio<2>, std::ratio<0>>;
	using MillimeterSquared = Quantity<std::ratio<0>, std::ratio<2,1000>, std::ratio<0>>;
	using CentimeterSquared = Quantity<std::ratio<0>, std::ratio<2,100>, std::ratio<0>>;
	using KilometerSquared = Quantity<std::ratio<0>, std::ratio<2000>, std::ratio<0>>;

	// Time
	using Second = Quantity<std::ratio<0>, std::ratio<0>, std::ratio<1>>;
	using Millisecond = Quantity<std::ratio<0>, std::ratio<0>, std::ratio<1,1000>>;
	using Microsecond = Quantity<std::ratio<0>, std::ratio<0>, std::ratio<1,10000>>;
	using Minute = Quantity<std::ratio<0>, std::ratio<0>, std::ratio<10>>;
	using Hour = Quantity<std::ratio<0>, std::ratio<0>, std::ratio<100>>;

	// Speed
	using MeterPerSecond = Quantity<std::ratio<0>, std::ratio<1>, std::ratio<-1>>;
	using KilometerPerHour = Quantity<std::ratio<0>, std::ratio<1000>, std::ratio<-100>>;

	// Pressure
	using GramPerMeterCubic = Quantity<std::ratio<1>, std::ratio<-3>, std::ratio<0>>;
	using GramPerMilliMeterCubic = Quantity<std::ratio<1>, std::ratio<-3, 1000>, std::ratio<0>>;
}

#define PSC_PHYSICAL_QUANTITY Quantity<M,L,T>
#define PSC_NO_LENGTH std::is_same< L, std::ratio<0> >::value
#define PSC_NO_TIME std::is_same< T, std::ratio<0> >::value
#define PSC_NO_MASS std::is_same<M, std::ratio<0>>::value

#define PSC_WITH_MASS std::ratio_greater<M, std::ratio<0>>::value
#define PSC_WITH_LENGTH std::ratio_greater<L, std::ratio<0> >::value
#define PSC_WITH_TIME std::ratio_greater<T, std::ratio<0> >::value
#define PSC_WITH_TIME_DENOM std::ratio_less<T, std::ratio<0> >::value

#define PSC_ENSURE_WEIGHT  template < typename M, typename L, typename T, typename = typename std::enable_if< PSC_WITH_MASS && PSC_NO_LENGTH && PSC_NO_TIME >::type >

#define PSC_PHYSICAL_UNIT template < typename M, typename L, typename T>
