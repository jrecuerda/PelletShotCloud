#pragma once

#include <cassert>
#include <cmath>

namespace PSC
{

template <typename T>
class vec3{
public:
	constexpr vec3(T x, T y, T z) : x{x}, y{y}, z{z} {}
	vec3() : x{0.0}, y{0.0}, z{0.0} {}
	~vec3() = default;
	vec3(vec3 const&) = default;
	vec3(vec3&&) = default;
	vec3& operator=(vec3 const&) = default;
	vec3& operator=(vec3 &&) = default;

	constexpr void set(T x, T y, T z) noexcept;

	constexpr T mod() const noexcept;
	constexpr vec3 norm() const noexcept;
	constexpr T dot(vec3 const&) const noexcept;
	static constexpr vec3 cross(vec3 const& lhs, vec3 const& rhs) noexcept;

	constexpr vec3 operator+(vec3 const&) const noexcept;
	constexpr vec3 operator+(T) const noexcept;
	constexpr vec3& operator+=(vec3 const&) noexcept;
	constexpr vec3& operator+=(T) noexcept;

	constexpr vec3 operator-(vec3 const&) const noexcept;
	constexpr vec3 operator-(T) const noexcept;
	constexpr vec3& operator-=(vec3 const&) noexcept;
	constexpr vec3& operator-=(T) noexcept;

	constexpr vec3 operator*(vec3 const&) const noexcept;
	constexpr vec3 operator*(T) const noexcept;
	constexpr vec3& operator*=(vec3 const&) noexcept;
	constexpr vec3& operator*=(T) noexcept;

	constexpr vec3 operator/(vec3 const&) const noexcept;
	constexpr vec3 operator/(T) const noexcept;
	constexpr vec3& operator/=(vec3 const&) noexcept;
	constexpr vec3& operator/=(T) noexcept;

	T x, y , z;
};

template <typename T>
constexpr
void vec3<T>::set(T newX, T newY, T newZ) noexcept
{
	x = newX;
	y = newY;
	z = newZ;
}

template <typename T>
constexpr
T vec3<T>::mod() const noexcept
{
	using namespace std;
	return sqrt(pow(x,2) + pow(y,2) + pow(z,2));
}

template <typename T>
constexpr
vec3<T> vec3<T>::norm() const noexcept
{
	assert(std::abs(mod()) > 1e-12 && "Normalize a 0 vector is not possible");
	return operator/(mod());
}

template <typename T>
constexpr
T vec3<T>::dot(vec3 const& rhs) const noexcept
{
	return (x * rhs.x + y * rhs.y + z * rhs.z);
}

template <typename T>
constexpr
vec3<T> vec3<T>::cross(vec3<T> const& lhs, vec3<T> const& rhs) noexcept
{
	return vec3<T>(
				lhs.y*rhs.z - lhs.z*rhs.y,
				lhs.z*rhs.x - lhs.x*rhs.z,
				lhs.x*rhs.y - lhs.y*rhs.x);
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator+(vec3<T> const& rhs) const noexcept
{
	return vec3<T>(x+rhs.x, y+rhs.y, z+rhs.z);
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator+(T rhs) const noexcept
{
	return vec3<T>(x+rhs, y+rhs, z+rhs);
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator+=(vec3<T> const& rhs) noexcept
{
	x+=rhs.x;
	y+=rhs.y;
	z+=rhs.z;
	return *this;
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator+=(T rhs) noexcept
{
	x+=rhs;
	y+=rhs;
	z+=rhs;
	return *this;
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator-(vec3<T> const& rhs) const noexcept
{
	return vec3<T>(x-rhs.x, y-rhs.y, z-rhs.z);
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator-(T rhs) const noexcept
{
	return vec3<T>(x-rhs, y-rhs, z-rhs);
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator-=(vec3<T> const& rhs) noexcept
{
	x-=rhs.x;
	y-=rhs.y;
	z-=rhs.z;
	return *this;
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator-=(T rhs) noexcept
{
	x-=rhs;
	y-=rhs;
	z-=rhs;
	return *this;
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator*(vec3<T> const& rhs) const noexcept
{
	return vec3<T>(x*rhs.x, y*rhs.y, z*rhs.z);
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator*=(vec3<T> const& rhs) noexcept
{
	x*=rhs.x;
	y*=rhs.y;
	z*=rhs.z;
	return *this;
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator*(T rhs) const noexcept
{
	return vec3<T>(x*rhs, y*rhs, z*rhs);
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator*=(T rhs) noexcept
{
	x*=rhs;
	y*=rhs;
	z*=rhs;
	return *this;
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator/(vec3<T> const& rhs) const noexcept
{
	return vec3<T>(x/rhs.x, y/rhs.y, z/rhs.z);
}

template <typename T>
constexpr
vec3<T> vec3<T>::operator/(T rhs) const noexcept
{
	return vec3<T>(x/rhs, y/rhs, z/rhs);
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator/=(vec3<T> const& rhs) noexcept
{
	x/=rhs.x;
	y/=rhs.y;
	z/=rhs.z;
	return *this;
}

template <typename T>
constexpr
vec3<T>& vec3<T>::operator/=(T rhs) noexcept
{
	x/=rhs;
	y/=rhs;
	z/=rhs;
	return *this;
}

}
