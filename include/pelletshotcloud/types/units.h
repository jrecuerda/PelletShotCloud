#pragma once

#include "types/psc.h"

#include <cmath>
#include <ratio>

namespace PSC
{
	// This is not intended to be a whole unit system since the library
	// internaly will work with SI. It is thought as a way to make
	// API strong and understandable

	template <typename MassQ, typename LengthQ, typename TimeQ>
	class Quantity
	{
	private:
		Real _value;

	public:
		constexpr Quantity() : _value(0.0) {}
		constexpr Quantity(Real val) : _value(val) {}

		constexpr Real get() const noexcept
		{	
			return _value;
		}

		constexpr Real get_is() const noexcept
		{
			Real retValue = _value;

			convertIS<MassQ>(retValue);
			convertIS<LengthQ>(retValue);
			convertISTime(retValue);

			return retValue;
		}

		template< typename Type>
		constexpr void convertIS(Real& value) const noexcept
		{
			Real ratio = 0.0;
			Real num = Type::num;
			Real den = Type::den;
			if (num != 0 && den != 0)
			{
				ratio = num / den;
				auto powered_ratio = getPoweredRatio(ratio);
				value *= ratio > 0 ? powered_ratio : 1 / powered_ratio;
			} 
		}

		constexpr Real getPoweredRatio(Real ratio) const noexcept
		{
			// if 1000 return 1000
			// if 200 return 2. It menass squred -> 100^2
			// if 0.003 return 3. It menas Cubic -> 1/(1000^3)
			Real abs_ratio = std::abs(ratio);
			Real num_decimals = std::floor(std::log10(abs_ratio));
			return std::pow(std::pow(10, num_decimals), abs_ratio/std::pow(10, num_decimals) );
		}

		constexpr void convertISTime(Real& value) const noexcept
		{
			Real ratio = 0.0;
			Real num = TimeQ::num;
			Real den = TimeQ::den;
			if (num != 0 && den != 0)
			{
				ratio = num / den;
				if (std::abs(ratio) < 1.00001)
				{
					value *= ratio > 0 ? ratio : 1 / std::abs(ratio);
				}
				else
				{
					// Minutes = 10
					// Hours = 100
					// Days not supported
					Real multipier_applied_ratio = std::pow(60, std::log10(std::abs(ratio)));
					value *= ratio > 0 ? multipier_applied_ratio : 1 / multipier_applied_ratio;
				}
			} 
		}
	};
}
