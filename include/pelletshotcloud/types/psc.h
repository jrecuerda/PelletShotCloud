#pragma once

#include "types/vec3.h"

namespace PSC 
{ 
#ifdef REAL_AS_FLOAT
   using Real = float;
#else
	using Real = double;
#endif
   using vec3real = vec3<Real>;
}
