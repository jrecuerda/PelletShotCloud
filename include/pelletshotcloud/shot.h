#pragma once

#include "cartridge.h"
#include "flying_object.h"

#include <memory>
#include <vector>

namespace PSC
{
class Shot
{
public:
	using PelletVector = std::vector<FlyingObject>;

	Shot();
	~Shot() noexcept;
	Shot(Shot const&);
	Shot& operator=(Shot const&);
	Shot(Shot &&) noexcept;
	Shot& operator=(Shot&&) noexcept;

	Shot(Cartridge const& cartridge, PelletVector const& pellets);
	Shot(Cartridge&& cartridge, PelletVector&& pellets);

	PelletVector const& getPellets() const;
	PelletVector& getPellets();

	vec3real getAvgPosition() const;
	vec3real getAvgVelocity() const;

private: 
	class Impl;
	std::unique_ptr<Impl> _impl;
};
}
