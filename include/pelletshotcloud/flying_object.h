#pragma once

namespace PSC
{
class FlyingObject
{
public:
	FlyingObject(vec3real const& pos, vec3real const& vel, Real dragCoeff)
		: position{pos}
		, velocity{vel}
		, dragCoeficient{dragCoeff}
	{}
	FlyingObject() = default;

	vec3real position;
	vec3real velocity;
	Real dragCoeficient;
};
}
