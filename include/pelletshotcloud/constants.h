#pragma once

namespace PSC
{
namespace Constants
{
	template<typename T>
	constexpr T pi = T(3.1415926535897932385);

	template<typename T>
	constexpr T gravity = T(9.80665);

	template<typename T>
	constexpr T soundSpeed = T(343);

	template<typename T>
	constexpr T transonicMarchLimit = T(0.63);

	template<typename T>
	constexpr T supersonicCdSlope = T(0.52/0.63);

	template<typename T>
	constexpr T airDensity = T(1'250);

	template<typename T>
	constexpr T subsonicCd = T(0.52);

	template<typename T>
	constexpr T edgeTrailCdFactor = T(1.22847563697152);
	
	template<typename T>
	constexpr T referenceStd40Yards3Stars = T(0.2400);

	template <typename T>
	constexpr T referenceDistance40Yards = T(36.576);
}
}
