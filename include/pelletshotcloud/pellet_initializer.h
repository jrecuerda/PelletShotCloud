#pragma once

#include "cartridge.h"

#include <memory>

namespace PSC
{
namespace Random{ class Randomizer; }
class Cartridge;
class FlyingObject;

class PelletInitializer
{
public:
	// TODO: Add choke as param
	PelletInitializer(Cartridge const& cartridge, vec3real direction) noexcept;

	FlyingObject initializePellet(Random::Randomizer& randomizer) const;

private:
	Cartridge _cartridge;
	vec3real _direction;
};
}
