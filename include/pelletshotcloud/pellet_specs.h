#pragma once

#include "types/psc.h"

#include <array>

namespace PSC
{
	enum class Material
	{
		LEAD
	};

	enum class PelletNumber
	{
		NUMBER_11 = 0,
		NUMBER_10,
		NUMBER_9,
		NUMBER_8,
		NUMBER_7_5,
		NUMBER_7,
		NUMBER_6,
		NUMBER_5,
		NUMBER_4,
		NUMBER_3,
		NUMBER_2,
		NUMBER_1,
		NUMBER_0,
		NUMBER_00,
		NUMBER_000,
		SLUG_6_2,
		SLUG_6_8,
		SLUG_7_4,
		SLUG_7_65,
		SLUG_8_65,
		COUNT
	};

	struct PelletSpecs
	{
		constexpr PelletSpecs(Real diameter, Real weight, Real dragCoefficientMaterialFactor) noexcept
			: diameter{diameter}
			, weight{weight}
			, dragCoefficientMaterialFactor{dragCoefficientMaterialFactor}
		{}

		Real diameter;
		Real weight;
		Real dragCoefficientMaterialFactor;
	};

	struct PelletsSpecs
	{
		static PelletSpecs const& get(PelletNumber number);
	};

	constexpr unsigned int computeNumberOfPellets(PelletSpecs const& specs, Real load)
	{
		return static_cast<unsigned int>(load / specs.weight);
	}

}
