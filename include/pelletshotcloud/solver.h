#pragma once

#include <shot.h>

#include <functional>
#include <memory>
#include <vector>

namespace PSC
{
class Solver
{
public:
	Solver(Shot shot);
	~Solver() noexcept;
	Solver(Solver const&);
	Solver(Solver&&) noexcept;
	Solver& operator=(Solver const&);
	Solver& operator=(Solver&&) noexcept;

	struct Snapshot
	{
		Snapshot(Real t, Real d, Real v, Shot const& s) noexcept
			: time{t}
			, distance{d}
			, velocity{v}
			, shot{s}
		{}

		Real time;
		Real distance;
		Real velocity;
		Shot shot;
	};
	using Storage = std::vector<Snapshot>;

	Storage solveFullPath();
	Shot const* solveStep(Real dt);

	void setTimeFraction(Real time);
	void setDistanceStopCondition(std::function<bool(Real)> conditionFunc);
	void setVelocityStopCondition(std::function<bool(Real)> conditionFunc);
	void setTimeStopCondition(std::function<bool(Real)> conditionFunc);

private:
	struct Impl;
	std::unique_ptr<Impl> _impl;
};
}
