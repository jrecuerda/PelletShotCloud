#pragma once

#include "types/physical_units.h"
#include "pellet_specs.h"

#include <ratio>

namespace PSC
{
struct PelletSpecs;

class Cartridge
{
public:

	PSC_PHYSICAL_UNIT
	Cartridge(PelletNumber number, PSC_PHYSICAL_QUANTITY load) noexcept
		: _load{load.get_is()}
		, _pelletSpecs{PelletsSpecs::get(number)}
	{
	   static_assert(PSC_WITH_MASS && PSC_NO_LENGTH && PSC_NO_TIME, 
		   "Weight unit is required");
		_numberOfPellets = computeNumberOfPellets(_pelletSpecs, _load);
	}

	PSC_PHYSICAL_UNIT
	void setInitialSpeed(PSC_PHYSICAL_QUANTITY speed)
	{
	   static_assert(PSC_WITH_LENGTH && PSC_WITH_TIME_DENOM && PSC_NO_MASS,
		   "Speed unit is required");
	   _initialSpeed = speed.get_is();
	}

	unsigned int getNumberOfPellets() const noexcept;
	Real getInitialSpeed() const noexcept;
	Real getDragCoefficientMaterialFactor() const noexcept;

private:
	Real _load;
	unsigned int _numberOfPellets;
	PelletSpecs _pelletSpecs;
	Real _initialSpeed = 440.0;
};
}
