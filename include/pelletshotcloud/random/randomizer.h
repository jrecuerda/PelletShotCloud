#pragma once

#include "constants.h"
#include "random/rayleigh.h"

#include <random>

namespace PSC
{
namespace Random
{
class Randomizer
{
public:
	explicit Randomizer(Real scale);
	Randomizer(Randomizer const&) = delete;
	Randomizer& operator=(Randomizer const&) = delete;
	
	void setScale(Real scale) noexcept;

	inline Real getRho() noexcept
	{
		return _rayleigh(_gen);
	}

	inline Real getTheta() noexcept
	{
		return _uniform(_gen);
	}

	inline Real getDeformationFactor(Real rho) noexcept
	{
		Real norm_rho = rho/(2.0*_scale);
		std::normal_distribution<Real> dist(norm_rho, 0.1+norm_rho/8.0);
		return 1.0 + dist(_gen)*(Constants::edgeTrailCdFactor<Real>-1.0)/1.5;
	}


private:
   RayleighDistribution<Real> _rayleigh;
   std::mt19937 _gen;
   std::uniform_real_distribution<Real> _uniform;
	Real _scale;	
};
}
}
