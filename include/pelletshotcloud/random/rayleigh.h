#pragma once

#include "types/psc.h"

#include <random>
#include <cmath>

namespace PSC
{
namespace Random
{
template<typename RealType = Real>
class RayleighDistribution
{
public:
	explicit RayleighDistribution(RealType scale) noexcept 
		: _baseDistribution{RealType(0.0), RealType(1.0)}
		, _scale{scale}
	{}

	template <typename Generator>
	RealType operator()(Generator& gen)
	{
		return std::sqrt(-2 * std::pow(_scale,2) * std::log(_baseDistribution(gen)));
	}

private:
	std::uniform_real_distribution<RealType> _baseDistribution;
	RealType _scale;
};
}
}
