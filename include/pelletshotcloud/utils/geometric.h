#pragma once

#include "types/vec3.h"

#include <cmath>

namespace PSC
{
namespace Utils
{
//-------- Definitions
template<typename T>
constexpr
vec3<T> rotateVector(vec3<T> const& rotationAxis, vec3<T> const& vectorToRotate, T angle);

template<typename T>
constexpr
vec3<T> pickAnyPerpendicularVector(vec3<T> const& perpendicualrTo);

template<typename T>
constexpr
vec3<T> getVectorRotatedInPlane(vec3<T> const& planeNormalVec, T angle);

template<typename T>
constexpr
void movePoint(vec3<T> const& direction, vec3<T>& point, T distance);

//-------- Implementations

template<typename T>
constexpr
vec3<T> rotateVector(vec3<T> const& rotationAxis, vec3<T> const& vectorToRotate, T angle)
{
	// Rodrigue's rotation forumla
	// https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
	return vectorToRotate * std::cos(angle) +
			(vec3<T>::cross(rotationAxis, vectorToRotate) * std::sin(angle)) +
			rotationAxis * (rotationAxis.dot(vectorToRotate) * (1 - std::cos(angle)));
}

template<typename T>
constexpr
vec3<T> pickAnyPerpendicularVector(vec3<T> const& perpendicualrTo)
{
	if(perpendicualrTo.x == 0)
	{
		return vec3<T>::cross(perpendicualrTo, vec3<T>(1.0, 0.0, 0.0));
	}
	else
	{
		return vec3<T>::cross(perpendicualrTo, vec3<T>(0.0, 0.0, 1.0));
	}
}

template<typename T>
constexpr
vec3<T> getVectorRotatedInPlane(vec3<T> const& planeNormalVec, T angle)
{
	vec3<T> vectorInPlane = pickAnyPerpendicularVector(planeNormalVec);
	return rotateVector(planeNormalVec, vectorInPlane, angle);
}

template<typename T>
constexpr
void movePoint(vec3<T> const& direction, vec3<T>& point, T distance)
{
	point += direction.norm() * distance;
}

}
}
